<!DOCTYPE html>
<html lang ="en">
  <head>
    <meta charset="utf-8">
    <title>Main page</title>
    <link href="templates/css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body>
    <?php require_once("header.php");?>
      <div class="container">
        <div class="row">
          <div class="col-md-1"></div>
  				  <div class="col-md-4">
              <blockquote>
                <img src="pictures/1.jpg" width="100%"class="img-thumbnail">
              </blockquote>
  				  </div>
            <div class="col-md-4">
				      <div class="panel panel-default">
                <div class="panel-heading"><h3>About Me...</h3></div>
  						    <div class="panel-body">
    						    <dl>
  								    <dt>Name</dt>
  								    <dd>Mikhail</dd>
  							   	  <dt>Surname</dt>
  						  		  <dd>K.</dd>
  					   			  <dt>A2Study</dt>
  	                </dl>
  				  		  </div>
				     </div>			
			    </div>
		    </div>
      </div>
    <?php require_once("footer.php");?>
  <script src="templates/js/jquery-latest.js"></script>
  <script src="templates/js/bootstrap.min.js"></script>
  </body>
</html>