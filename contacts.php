<?php
  if (!empty($_POST)) 
  {
    if (!filter_var($_POST[email], FILTER_VALIDATE_EMAIL)) 
      {
        $_POST[email_error] = true;
      } 
      else
        if (empty($_POST[text]) || (strlen($_POST[text]) > 500)) 
          {
            $_POST[message_error] = true;
          }
          else
          {
            $_POST[post] = true;
            $csvFile = 'data/message.csv';
            $csvData = "Name: {$_POST['name']} | Email:  {$_POST['email']} | Text:  {$_POST['text']}\r\n";
            file_put_contents( $csvFile, $csvData, FILE_APPEND );
          }
    } 
    else 
    {
      $_POST[post] = false;
    }
?>

<!DOCTYPE html>
<html lang ="en">
  <head>
    <meta charset="utf-8">
    <title>Connect</title>
    <link href="templates/css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body>
    <?php require_once("header.php");?>
      <div class="container">
        <form action="" method="post">
          <blockquote>
            <div class="row">
              <div class="col-md-4">
                <h4>Name:</h4> <input name="name" class="form-control" placeholder="Your name" value="<?php if (!$_POST[post]) echo $_POST[name]; ?>" /><br/>
                <h4>Email:</h4> <input name="email" class="form-control" placeholder="Your email" value="<?php if (!$_POST[post]) echo $_POST[email]; ?>"/></br>
                <?php 
                  if ($_POST[email_error]) echo "<input class=\"bg-danger\" value=\"Enter correct email!\" readonly";
                ?>
                <br/>
                </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <h4>Message:</h4><textarea name="text" class="form-control" placeholder="Your message" rows="5"><?php
                    if (!$_POST[post]) echo $_POST[text];
                    ?></textarea></br>
                  <?php
                    if ($_POST[message_error]) echo "<input class=\"bg-danger\" value=\"Enter no more 500 symbol's!\"readonly";
                  ?>
                  </br>
                  </br>
                  </br>
                  <input name="submit" type="submit" class="btn btn-primary" value="Send message"/>
              </div>
            </div>
          </blockquote>
        </form>
      </div>
    <?php require_once("footer.php");?>
  <script src="templates/js/jquery-latest.js"></script>
  <script src="templates/js/bootstrap.min.js"></script>
  </body>
</html>